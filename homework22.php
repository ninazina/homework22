<?php
echo "Задание 1: 
  Создать класс Студент, у которого есть поля Имя, Фамилия, Возраст, Курс.
1.1 Конструктор класса, который принимает значения для всех полей и устанавливает их соответствующим образом. 
1.2 Методы для получения значений полей (геттеры).
1.3 Методы для установки значений полей (сеттеры).  
1.4 Методы для вывода информации о студенте.  1.5 Вывести всю информацию.<br> ";
class Student {
    public $name;
    public $secondname;
    public $age;
    public $course;
    public function __construct ($name, $secondname, $age, $course) {
        $this->name = $name;
        $this->secondname = $secondname;
        $this->age = $age;
        $this->course = $course;
    }
    public function getName ($name){
        return $this->name;
    }
    public function getSecondname ($secondname){
        return $this->secondname;
    }
    public function getAge ($age){
        return $this->age;
    }
    public function getCourse ($course){
        return $this->course;
    }
    public function setName ($name){
        $this->name = $name;
    }
    public function setSecondname ($secondname){
        $this->secondname = $secondname;
    }
    public function setAge ($age){
        $this->age = $age;
    }
    public function setCourse ($course){
        $this->course = $course;
    }
    public function getInformation (){
        return "Меня зовут " . $this->name ." " . $this->secondname . ", мне " .$this->age . " лет и я учусь на " . $this->course . " курсе.";
    }
}
$student1= new Student ('Fedor', 'Orlow', 25, 2);
$student1-> setName('Fedor');
$student1-> setSecondname ('Orlow');
$student1-> setAge (25);
$student1-> setCourse (2);
echo $student1-> getInformation ();




echo '<br>';
echo "<br> Задание 2: 
   Создать класс Автомобиль, у которого есть поля Марка, Модель, Цвет, Год выпуска.

2.1 Конструктор класса, который принимает значения для всех полей и устанавливает их соответствующим образом.
2.2 Методы для получения значений полей (геттеры)
2.3 Методы для установки значений полей (сеттеры)
2.4 Методы для вывода информации об автомобиле.<br>";

class Car {
    public function __construct (
        public $name,
        public $model,
        public $color,
        public $year,) {
        $this->name = $name;
        $this->model = $model;
        $this->color = $color;
        $this->year = $year;
    }
    public function getName ($name){
        $this->name;
    }
    public function getModel ($model){
        $this->model;
    }
    public function getColor ($color){
        $this->color;
    }
    public function getYear ($year){
        $this->year;
    }
    public function setName ($name){
        $this->name = $name;
    }
    public function setModel ($model){
        $this->model = $model;
    }
    public function setColor ($color){
        $this->color = $color;
    }
    public function setYear ($year){
        $this->year = $year;
    }
    public function getInfo (){
        return "Автомобиль марки " . $this->name .", модель " . $this->model . ". Цвет- " .$this->color . ", год выпуска: " . $this->year . ".";
    }
}
$car1= new Car ('Audi', 'A6', 'red', 2020);
$car1-> setName('Audi');
$car1-> setModel ('A6');
$car1-> setColor ('red');
$car1-> setYear (2020);
echo $car1-> getInfo ();



echo '<br>';
echo "<br>Задание 3: 
   Создать класс Круг, у которого есть поле Радиус.

3.1 Конструктор класса, который принимает значение радиуса и устанавливает его соответствующим образом.
3.2 Метод для вычисления площади круга - calculateArea(). <br>";

class Circle {
    public function __construct (public $radius) {
        $this->radius = $radius;
    }

    public function calculateArea (){
        return 3.1415926 * pow($this->radius, 2);
    }
}
$circle= new Circle (15);
echo $circle-> calculateArea ();

echo '<br>';
echo "<br> Задание 4: 
   Создать класс Человек, у которого есть поля Имя, Возраст, Пол.
Написать методы для работы с этими полями:

4.1 Конструктор класса, который принимает значения для всех полей и устанавливает их соответствующим образом.
4.2 Метод для проверки, является ли человек совершеннолетним - isAdult(), который возвращает true, если возраст человека больше или равен 18,
 и false в противном случае.<br>";

class Person {
    public function __construct (
        public $name,
        public $age,
        public $gender,) {
        $this->name = $name;
        $this->age = $age;
        $this->gender = $gender;
    }
    public function isAdult (){
        if ($this->age>=18){
            return "Вы совершеннолетний";
        } else {
            return "Вы несовершеннолетний";
        }
    }
}
$person= new Person ("Olya", 12, "W");
echo $person-> isAdult ();

